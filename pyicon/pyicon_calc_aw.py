import warnings
import numpy as np
import xarray as xr
from itertools import product
from scipy.spatial import cKDTree
from scipy.spatial import Delaunay
import os


class daskicon:
    def __init__(self, grid2d, zarr_fpath_tgrid, zarr_fpath_converted, zarr_fpath_aux):
        """Initialize the daskicon class."""
        
        ## Load / Compute T-Grid Data
        if not os.path.exists(str(zarr_fpath_tgrid)):
            from pyicon.pyicon_tb import identify_grid
            from pyicon.pyicon_params import params
            path_grid = params['path_grid']
            Dgrid = identify_grid(grid2d, path_grid, uuidOfHGrid=grid2d.attrs['uuidOfHGrid'])
            fpath_tgrid = Dgrid['fpath_grid']
            ds_tg = xr.open_dataset(fpath_tgrid, chunks={})
            ds_tg.to_zarr(zarr_fpath_tgrid, mode='w')

        if not os.path.exists(str(zarr_fpath_converted)):
            ds_tg = xr.open_zarr(str(zarr_fpath_tgrid))
            ds_IcD = self.convert_tgrid_data(ds_tg)
            ds_IcD.to_zarr(zarr_fpath_converted, mode='w')
        
        
        self.ds_IcD = xr.open_zarr(zarr_fpath_converted, chunks={})
        self.grid2d = grid2d
        
        if not os.path.exists(str(zarr_fpath_aux)):
            ds_aux = self.calculate_aux_data()
            ds_aux.to_zarr(zarr_fpath_aux, mode='w')
        
        self.ds_aux = xr.open_zarr(zarr_fpath_aux, chunks={})
        
        self.mask_edge = grid2d.edge_sea_land_mask
        self.mask = grid2d.cell_sea_land_mask.rename({'clat':'lat', 'clon':'lon'})
        
        return
    
    
    ## Interpolator Functions:
    #   These Interpolators take arbitrary data and put it onto the present ICON grid of the daskicon object
    
    def make_nn_interpolator(self, da):
        
        points_icon_source = np.vstack((da.lon, da.lat)).T
        points_icon_target = np.vstack((self.grid2d.clon*180./np.pi, self.grid2d.clat*180./np.pi)).T
        
        tree = cKDTree(points_icon_source)
        _, nn_indices_ao = tree.query(points_icon_target)
        
        return nn_indices_ao
    
    def interp_nn(self, da_source, interpolator):
        
        indices = interpolator
        da_on_target = da_source.isel(ncells=indices)
        return da_on_target

    def make_linear_interpolator(self, da):
        
        points_icon_source = np.vstack((da.lon, da.lat)).T
        points_icon_target = np.vstack((self.grid2d.clon*180./np.pi, self.grid2d.clat*180./np.pi)).T
        
        tri = Delaunay(points_icon_source)

        # Find simplices containing target points and get valid indices
        simplices = tri.find_simplex(points_icon_target)
        indices = np.where(simplices >= 0)[0]
        valid_simplices = simplices[indices]

        # Get the vertices and compute barycentric coordinates
        vertices = tri.simplices[valid_simplices]
        delta = points_icon_source[vertices] - points_icon_target[indices, None, :]

        # Compute barycentric coordinates
        weights = np.einsum('ijk,ik->ij', 
                        delta[:, :-1] - delta[:, -1:], 
                        -delta[:, -1]) / np.einsum('ijk,ijk->ij', 
                                                    delta[:, :-1] - delta[:, -1:], 
                                                    delta[:, :-1] - delta[:, -1:])
        weights = np.c_[weights, 1 - weights.sum(axis=1)]

        return indices, vertices, weights
    
    def interp_linear(self, da_source, interpolator):

        indices, vertices, weights = interpolator
        n_points = max(indices.max() + 1, len(da_source.ncells))
        
        def interp_timestep(values):
            
            n_times = values.shape[0]
            result = np.full((n_times, n_points), np.nan)
            
            # Get values at vertices: shape becomes (n_times, n_target_points, n_vertices)
            vertex_values = values[:, vertices]
            
            # Multiply by weights and sum: (n_times, n_target_points)
            result[:, indices] = np.sum(vertex_values * weights, axis=2)
            
            return result
        
        
        result = xr.apply_ufunc(
            interp_timestep,
            da_source.transpose('time','ncells'),
            input_core_dims=[['ncells']],
            output_core_dims=[['ncells_target']],
            vectorize=False,
            dask='parallelized',
            output_dtypes=[np.float32],
            dask_gufunc_kwargs={'output_sizes': {'ncells_target': n_points}}
        )
        
        result = result.rename({'ncells_target':'ncells'})
        result = result.assign_coords({
            'lat': ('ncells', self.grid2d.clat.data*180./np.pi),
            'lon': ('ncells', self.grid2d.clon.data*180./np.pi)
            })
        
        return result
    
    
    
        
    
    ## High-level Vector Operations
    def compute_curl_cells(self, ds_uv):
        """Computes the curl of a vector field and returns on cell centres"""
        
        ds_uv = ds_uv.rename({'ncells':'cell'})
        lat = ds_uv.lat
        lon = ds_uv.lon
                
        ## Calculate Curl
        uv_cartesian = self.calc_3d_from_2dlocal(ds_uv.u, ds_uv.v)               # Project to cartesian coordinates
        uv_cartesian_edges = self.cell2edges(uv_cartesian)                       # Put data onto edges
        uv_cartesian_edges_masked = uv_cartesian_edges.where(self.mask_edge <= -1.0, other=0)  ## (IMPORTANT): Mask out the land boundaries !!!
        curl_uv_vertex = self.calc_curl(uv_cartesian_edges_masked)               # Calculate curl
        curl_uv = self.vertex2cell(curl_uv_vertex)                               # Put data back onto cells

        # Clean up data array
        curl_uv = curl_uv.drop_vars(['clat','clon','vlat','vlon']).assign_coords({'lat':lat, 'lon':lon}).rename({'cell':'ncells'})
        curl_uv = curl_uv.transpose('time','ncells')
        
        # Mask out land / boundary region & limit to significant values
        curl_uv_mask = curl_uv.where(self.mask < 0)  # < 0 Removes all Land.  < -1 Removes also the 1st boundary ocean cell
        
        return curl_uv_mask
    
    
    def compute_grad_cells(self, da):
        """Computes the gradient of a scalar field and returns on cell centres"""
        
        da = da.rename({'ncells':'cell'})
        grad_edges = self.calc_grad(da)
        grad = self.edges2cell(grad_edges)
        
        dadx, dady = self.calc_2dlocal_from_3d(grad)
        dadx = dadx.rename({'cell':'ncells'})
        dady = dady.rename({'cell':'ncells'})

        return dadx, dady
    
    
    
    
    
    
    
    ## Functions to map between 3D Cartesian and 2D local vectors
    def calc_2dlocal_from_3d(self, p_vn_c):
        """Transform vector from cartesian to spherical basis at a cell center

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        p_vn_c : xr.Dataset
            dataset containing cartesian representation of
            vector. Should have the dimension 'cart'


        Returns
        -------
        uo : xr.DataArray
            zonal component of vector

        vo : xr.DataArray
            meridional component of vector


        Notes
        -----
        The 3D vector passed is not (u, v, w) where w is the local
        vertical.

        """
        clon_rad = self.ds_IcD.clon * np.pi / 180.0
        clat_rad = self.ds_IcD.clat * np.pi / 180.0

        sinLon = np.sin(clon_rad)
        cosLon = np.cos(clon_rad)
        sinLat = np.sin(clat_rad)
        cosLat = np.cos(clat_rad)

        u1 = p_vn_c.isel(cart=0)
        u2 = p_vn_c.isel(cart=1)
        u3 = p_vn_c.isel(cart=2)

        uo = u2 * cosLon - u1 * sinLon
        vo = -(u1 * cosLon + u2 * sinLon) * sinLat + u3 * cosLat

        return uo, vo


    def calc_3d_from_2dlocal(self, uo, vo):
        """Transform vector from spherical to cartesian basis at a cell center

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        uo : xr.DataArray
            zonal component

        vo : xr.DataArray
            meridional component

        Returns
        -------
        p_vn_c : xr.DataArray
            representation of the horizontal vector in a cartesian basis.


        Notes
        -----
        The components of the returned vector *are not* zonal, meridional and vertical.
        This function transforms a locally horizontal vector from a spherical representation
        to a cartesian representation.

        """
        
        clon_rad = self.ds_IcD.clon * np.pi / 180.0
        clat_rad = self.ds_IcD.clat * np.pi / 180.0

        sinLon = np.sin(clon_rad)
        cosLon = np.cos(clon_rad)
        sinLat = np.sin(clat_rad)
        cosLat = np.cos(clat_rad)

        u1 = -uo * sinLon - vo * sinLat * cosLon
        u2 = uo * cosLon - vo * sinLat * sinLon
        u3 = vo * cosLat

        p_vn_c = xr.concat([u1, u2, u3], dim="cart", coords="minimal")
        return p_vn_c





    ## Mapping between cells and edges
    def cell2edges(self, p_vn_c):
        """Remaps vector from cell center to edges

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        p_vn_c : xr.DataArray
            locally horizontal vector on cell center in cartesian
            representation.

        edge2cell_coeff_cc_t : xr.DataArray or None
            coefficients used in mapping cells to edges


        Returns
        -------
        ptp_vn : xr.DataArray
            vector p_vn_c remapped to edges
        """
        
        edge2cell_coeff_cc_t = self.ds_aux.edge2cell_coeff_cc_t.drop_vars({'clat','clon'})  # N.B.: These additional coordinates confuse dask !
        
        ic0 = self.ds_IcD.adjacent_cell_of_edge.isel(nc_e=0).compute()
        ic1 = self.ds_IcD.adjacent_cell_of_edge.isel(nc_e=1).compute()
        
        p_vn_c = p_vn_c.drop_vars(p_vn_c.coords)   # The additional coordinates create a lot of overhead....
        
        ptp_vn = (p_vn_c.isel(cell=ic0) * edge2cell_coeff_cc_t.isel(nc_e=0) + 
                  p_vn_c.isel(cell=ic1) * edge2cell_coeff_cc_t.isel(nc_e=1))  .sum(dim='cart')
        
        
        return ptp_vn.chunk({'edge':-1}) # Ensure still in a single chunk...


    ## Mapping between edges and cells
    def edges2cell(self, ve):
        """Remaps vector from edges to cell
        
        NOTE: AFW made this only for 2D (need to generalise for varying grid if not surface!)

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        ve : xr.DataArray
            vector on edges

        edge2cell_coeff_cc : xr.DataArray or None
            coefficients used in mapping edges to cells

        fixed_vol_norm : xr.DataArray or None
            volume of grid cell


        Returns
        -------
        p_vn_c : xr.DataArray
            cartesian representation of vector ve on cell centres
        """
        fixed_vol_norm = self.ds_aux.fixed_vol_norm
        edge2cell_coeff_cc = self.ds_aux.edge2cell_coeff_cc.chunk({'ne_c':-1})

        eoc = self.ds_IcD.edge_of_cell
        
        p_vn_c = xr.apply_ufunc(
            lambda v, ind, coeff: (v[ind] * coeff).sum(axis=1),
            ve,
            eoc,
            edge2cell_coeff_cc,
            input_core_dims=[['edge'], ['cell','ne_c'], ['cell','ne_c']],
            output_core_dims=[['cell']],
            vectorize=True,
            dask='parallelized',
            output_dtypes=[ve.dtype],
            dask_gufunc_kwargs={'output_sizes':{'cell': eoc.cell.size}}
        )

        p_vn_c = p_vn_c / fixed_vol_norm
               
        return p_vn_c



    ## Divergence
    def calc_div(self, vector):
        """Calculates coefficients for calculating divergence

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        vector : xr.DataArray
            vector at cell edges

        div_coeff : xr.DataArray
            coefficients for calculating divergence

        Returns
        -------
        div_of_vector : xr.DataArray
            divergence of vector at cell centers

        """
        div_coeff = self.ds_aux.div_coeff
        div_of_vector = (vector.isel(edge=self.ds_IcD.edge_of_cell) * div_coeff).sum(dim="ne_c")
        return div_of_vector


    ## Gradient
    def calc_grad(self, scalar):
        """Calculates coefficients for calculating gradient

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        scalar : xr.DataArray
            scalar at cell center

        grad_coeff : xr.DataArray
            coefficients for calculating gradient


        Returns
        -------
        grad_of_scalar : xr.DataArray
            horizontal gradient of scalar at edges
        """
        
        grad_coeff = self.calc_grad_coeff()
        adj_cell = self.ds_IcD.adjacent_cell_of_edge
        
        grad_of_scalar = (
            scalar.isel(cell=adj_cell.isel(nc_e=1))
            - scalar.isel(cell=adj_cell.isel(nc_e=0))
        ) * grad_coeff.chunk({'edge':-1}) # Ensure still in a single chunk...
        
        grad_of_scalar = grad_of_scalar.where(self.ds_IcD.edge_sea_land_mask <= -1.0, other=0.0)  ## (IMPORTANT): Mask out the land boundaries !!!
        
        return grad_of_scalar


    # Curl
    def calc_curl(self, vector):
        """Calculates the vertical component of the curl

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info

        vector : xr.DataArray
            Dataarray containing vector variable on cell edges.

        rot_coeff : xr.DataArray or None
            Array containing dims ("vertex", "ne_v")


        Returns
        -------
        curl_vec : xr.DataArray
            vertical component of the curl of the vector defined on vertex points


        Notes
        -----
        We calculate the curl through the use of Stokes'/Green's theorem. A similar
        procedure can be used to calculate the horizontal components of the curl
        (oriented along cell edges and defined at intermediate Z levels.) This will
        be implemented in a future release.

        If you're using this function on large datasets, performance gains may be
        made if you play around with the dimension order.

        """
        assert "edge" in vector.dims

        rot_coeff = self.ds_aux.rot_coeff.chunk({'ne_v':-1})
        
        eov = self.ds_IcD.edges_of_vertex.compute()
        
        curl_vec = xr.apply_ufunc(
            lambda v, ind, coeff: (v[ind] * coeff).sum(axis=1),  # NOTE: This gives very large/wrong values when the vertex has _any_ neighbouring undefined cell....
            vector,
            eov,
            rot_coeff,
            input_core_dims=[['edge'], ['vertex','ne_v'], ['vertex','ne_v']],
            output_core_dims=[['vertex']],
            vectorize=True,
            dask='parallelized',
            output_dtypes=[vector.dtype]
        )
        return curl_vec



    def vertex2cell(self, var_on_vertex):
        """Converts vertices to cell centres
            AFW 2024
        """
        
        voc = self.ds_IcD.vertex_of_cell

        c0 = var_on_vertex.isel(vertex=voc.isel(nv_c=0))
        c1 = var_on_vertex.isel(vertex=voc.isel(nv_c=1))
        c2 = var_on_vertex.isel(vertex=voc.isel(nv_c=2))

        var_on_cells = (c0 + c1 + c2) / 3

        return var_on_cells



    
    
    
    
    
    
    ## Calculate Coefficients
    
    def calc_edge2cell_coeff_cc_t(self):
        """Calculates the cell to edge coefficients

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        edge2cell_coeff_cc_t : xr.DataArray
            coefficients used in mapping cells to edges
        """
        dist_vector = self.ds_IcD.edge_cart_vec - self.ds_IcD.cell_cart_vec.compute().isel(
            cell=self.ds_IcD.adjacent_cell_of_edge.compute()
        )
        orientation = (dist_vector * self.ds_IcD.edge_prim_norm).sum(dim="cart")
        dist_vector *= np.sign(orientation)
        edge2cell_coeff_cc_t = (
            self.ds_IcD.edge_prim_norm
            * self.ds_IcD.grid_sphere_radius
            * np.sqrt((dist_vector**2).sum(dim="cart"))
            / self.ds_IcD.dual_edge_length
        )
        return edge2cell_coeff_cc_t.chunk({'cart':1,'nc_e':1})
    

    def calc_edge2cell_coeff_cc(self):
        """Calculates the edge to cell coefficients

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        edge2cell_coeff_cc : xr.DataArray
            coefficients used in mapping edges to cells
        """
        dist_vector = (
            self.ds_IcD.edge_cart_vec.compute().isel(edge=self.ds_IcD.edge_of_cell.compute()) - self.ds_IcD.cell_cart_vec
        )
        edge2cell_coeff_cc = (
            dist_vector
            * self.ds_IcD.edge_length.compute().isel(edge=self.ds_IcD.edge_of_cell.compute())
            / self.ds_IcD.grid_sphere_radius
            * self.ds_IcD.orientation_of_normal
        )
        return edge2cell_coeff_cc.chunk({'cart':1,'ne_c':1})
    
    
    def calc_fixed_volume_norm(self):
        """Calculates the fixed volume

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        fixed_vol_norm : xr.DataArray
            volume of grid cell
        """
        dist_vector = (
            self.ds_IcD.edge_cart_vec.compute().isel(edge=self.ds_IcD.edge_of_cell.compute()) - self.ds_IcD.cell_cart_vec
        )
        norm = np.sqrt((dist_vector**2).sum(dim="cart"))
        fixed_vol_norm = (
            0.5
            * norm
            * self.ds_IcD.edge_length.compute().isel(edge=self.ds_IcD.edge_of_cell.compute())
            / self.ds_IcD.grid_sphere_radius
        )
        fixed_vol_norm = fixed_vol_norm.sum(dim="ne_c")
        return fixed_vol_norm
    
    
    def calc_div_coeff(self):
        """Calculates coefficients for calculating divergence

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        div_coeff : xr.DataArray
            coefficients for calculating divergence
        """
        div_coeff = (
            self.ds_IcD.edge_length.compute().isel(edge=self.ds_IcD.edge_of_cell.compute())
            * self.ds_IcD.orientation_of_normal
            / self.ds_IcD.cell_area
        )
        return div_coeff.chunk({'ne_c':1})


    def calc_grad_coeff(self):
        """Calculates coefficients for calculating gradient

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        grad_coeff : xr.DataArray
            coefficients for calculating gradient
        """
        grad_coeff = 1.0 / self.ds_IcD.dual_edge_length
        return grad_coeff
    
    
    def calc_rot_coeff(self):
        """Calculates coefficients used in calculating the curl

        Parameters
        ----------
        ds_IcD : xr.Dataset
            pyicon dataset containing coordinate info


        Returns
        -------
        curl_coeffs : xr.DataArray
            coefficients for calculating curls
        """
        curl_coeffs = (
            self.ds_IcD.edge_orientation
            * self.ds_IcD.dual_edge_length.compute().isel(edge=self.ds_IcD.edges_of_vertex.compute())
            / self.ds_IcD.dual_area
        )
        return curl_coeffs.chunk({'ne_v':1})
    
    
    
    def calculate_aux_data(self):
        """Calculates data needed for various calculations."""
        
        ds_aux = xr.Dataset()
        
        ds_aux['edge2cell_coeff_cc_t'] = self.calc_edge2cell_coeff_cc_t()
        ds_aux['edge2cell_coeff_cc'] = self.calc_edge2cell_coeff_cc()
        
        ds_aux['fixed_vol_norm'] = self.calc_fixed_volume_norm()
        
        ds_aux['div_coeff'] = self.calc_div_coeff()
        
        ds_aux['rot_coeff'] = self.calc_rot_coeff()
        
        return ds_aux
    
    
    
    
    


    def convert_tgrid_data(
        ds_tg_in, check_previous_conversion=True, set_dim_order=None, old_dim_behaviour=None
    ):
        """Convert xarray grid file to grid file compatible with pyicon function.

        Parameters
        ----------
        ds_tg_in : xr.Dataset
            raw, unprocessed tgrid

        check_previous_conversion : bool
            check whether the dataset has already been converted and raise an error
            if so

        set_dim_order : bool or list
            Transpose the dataset so dimensions appear in the standard pyicon
            order, or the order listed

        old_dim_behaviour : bool or None
            If True labels "nc", "ne" and "nv" will not be corrected. If False
            they will be corrected and the DeprecationWarning will be silenced. If
            None a DeprecationWarning is triggered. Note that setting
            old_dim_behaviour will render the returned ds_IcD incompatible with
            other pyicon functions.


        Returns
        -------
        ds_IcD : xr.Dataset
            A tgrid dataset compatible with pyicon functions


        Notes
        -----
        Open classical ICON grid file by:
        ds_tg = xr.open_dataset(fpath_tg, chunks=dict())

        Then convert by:
        ds_IcD = pyic.convert_tgrid_data(ds_tg)
        """

        # make deep copy of ds_tg_in to avoid glaobal modifications if during this function call
        ds_tg = ds_tg_in.copy(deep=True)

        if old_dim_behaviour is None:
            warnings.warn(
                "The default behaviour for convert_tgrid has recently \
                        changed. It now corrects the dimension labels 'nc', \
                        'ne' and 'nv'. You should check your code for references\
                        to these dimension labels and update things accordingly.\
                        If you wish to use the old dimension behaviour, please \
                        set `old_dim_behaviour=True`, however, note that the \
                        returned dataset will not be compatible with other \
                        calc_xr functions. To silence this warning, set \
                        `old_dim_behaviour=False`.",
                DeprecationWarning,
            )

        if check_previous_conversion:
            if "converted_tgrid" in ds_tg.attrs:
                raise ValueError(
                    "ds_tg has previously been converted by this function, \
                    applying the function again will lead to undocumented \
                    behaviour. To proceed, set 'check_previous_conversion=False'"
                )

        ds_IcD = xr.Dataset()

        # --- constants (from src/shared/mo_physical_constants.f90)
        ds_IcD["grid_sphere_radius"] = 6.371229e6
        ds_IcD["grav"] = 9.80665
        ds_IcD["earth_angular_velocity"] = 7.29212e-05
        ds_IcD["rho0"] = 1025.022
        ds_IcD["rhoi"] = 917.0
        ds_IcD["rhos"] = 300.0
        ds_IcD["sal_ref"] = 35.0
        ds_IcD["sal_ice"] = 5.0
        rcpl = 3.1733
        cpd = 1004.64
        ds_IcD["cp"] = (rcpl + 1.0) * cpd
        ds_IcD["tref"] = 273.15
        ds_IcD["tmelt"] = 273.15
        ds_IcD["tfreeze"] = -1.9
        ds_IcD["alf"] = 2.8345e6 - 2.5008e6  # [J/kg]   latent heat for fusion

        # --- distances and areas
        ds_IcD["cell_area"] = ds_tg["cell_area"]
        ds_IcD["cell_area_p"] = ds_tg["cell_area_p"]
        ds_IcD["dual_area"] = ds_tg["dual_area"]
        ds_IcD["edge_length"] = ds_tg["edge_length"]
        ds_IcD["dual_edge_length"] = ds_tg["dual_edge_length"]
        ds_IcD["edge_cell_distance"] = ds_tg["edge_cell_distance"].transpose()
        # --- neighbor information
        ds_IcD["vertex_of_cell"] = ds_tg["vertex_of_cell"].transpose() - 1
        ds_IcD["edge_of_cell"] = ds_tg["edge_of_cell"].transpose() - 1
        ds_IcD["vertices_of_vertex"] = ds_tg["vertices_of_vertex"].transpose() - 1
        ds_IcD["edges_of_vertex"] = ds_tg["edges_of_vertex"].transpose() - 1
        ds_IcD["edge_vertices"] = ds_tg["edge_vertices"].transpose() - 1
        ds_IcD["adjacent_cell_of_edge"] = ds_tg["adjacent_cell_of_edge"].transpose() - 1
        ds_IcD["cells_of_vertex"] = ds_tg["cells_of_vertex"].transpose() - 1
        ds_IcD["adjacent_cell_of_cell"] = ds_tg["neighbor_cell_index"].transpose() - 1
        # --- orientation
        ds_IcD["orientation_of_normal"] = ds_tg["orientation_of_normal"].transpose()
        ds_IcD["edge_orientation"] = ds_tg["edge_orientation"].transpose()
        ds_IcD["tangent_orientation"] = ds_tg["edge_system_orientation"].transpose()

        # --- masks
        ds_IcD["cell_sea_land_mask"] = ds_tg["cell_sea_land_mask"]
        ds_IcD["edge_sea_land_mask"] = ds_tg["edge_sea_land_mask"]

        # --- coordinates
        ds_IcD["cell_cart_vec"] = xr.concat(
            [
                ds_tg["cell_circumcenter_cartesian_x"],
                ds_tg["cell_circumcenter_cartesian_y"],
                ds_tg["cell_circumcenter_cartesian_z"],
            ],
            dim="cart",
        ).transpose()

        ds_IcD["vert_cart_vec"] = xr.concat(
            [
                ds_tg["cartesian_x_vertices"],
                ds_tg["cartesian_y_vertices"],
                ds_tg["cartesian_z_vertices"],
            ],
            dim="cart",
        ).transpose()

        ds_IcD["edge_cart_vec"] = xr.concat(
            [
                ds_tg["edge_middle_cartesian_x"],
                ds_tg["edge_middle_cartesian_y"],
                ds_tg["edge_middle_cartesian_z"],
            ],
            dim="cart",
        ).transpose()

        ds_IcD["dual_edge_cart_vec"] = xr.concat(
            [
                ds_tg["edge_dual_middle_cartesian_x"],
                ds_tg["edge_dual_middle_cartesian_y"],
                ds_tg["edge_dual_middle_cartesian_z"],
            ],
            dim="cart",
        ).transpose()

        ds_IcD["edge_prim_norm"] = xr.concat(
            [
                ds_tg["edge_primal_normal_cartesian_x"],
                ds_tg["edge_primal_normal_cartesian_y"],
                ds_tg["edge_primal_normal_cartesian_z"],
            ],
            dim="cart",
        ).transpose()

        for point, dim in product("ecv", ("lat", "lon")):
            coord = point + dim
            ds_IcD[coord] *= 180.0 / np.pi
            ds_IcD[coord].attrs["units"] = "degrees"

        ds_IcD["fc"] = (
            2.0 * ds_IcD.earth_angular_velocity * np.sin(ds_IcD.clat * np.pi / 180.0)
        )
        ds_IcD["fe"] = (
            2.0 * ds_IcD.earth_angular_velocity * np.sin(ds_IcD.elat * np.pi / 180.0)
        )
        ds_IcD["fv"] = (
            2.0 * ds_IcD.earth_angular_velocity * np.sin(ds_IcD.vlat * np.pi / 180.0)
        )

        try:
            ds_IcD = ds_IcD.rename({"ncells": "cell"})
        except ValueError:
            pass

        # Default dimension names are messy and often wrong. Let's rename them.
        dim_name_remappings = {
            "vertex_of_cell": {"nv": "nv_c"},
            "edge_vertices": {"nc": "nv_e"},
            "vertices_of_vertex": {"ne": "nv_v"},
            "edge_of_cell": {"nv": "ne_c"},
            "edges_of_vertex": {"ne": "ne_v"},
            "adjacent_cell_of_edge": {"nc": "nc_e"},
            "cells_of_vertex": {"ne": "nc_v"},
            "edge_cell_distance": {"nc": "nc_e"},
            "orientation_of_normal": {"nv": "ne_c"},
            "edge_orientation": {"ne": "ne_v"},
            "adjacent_cell_of_cell": {"nv": "nc_c"},
        }

        for variable in dim_name_remappings:
            ds_IcD[variable] = ds_IcD[variable].rename(dim_name_remappings[variable])

        ds_IcD.attrs["converted_tgrid"] = True
        ds_tg.attrs["converted_tgrid"] = True

        if set_dim_order is None:
            standard_order = ["cell", "vertex", "edge", "nc", "nv", "ne", "cart", ...]
            ds_IcD = ds_IcD.transpose(*standard_order, missing_dims="ignore")
        elif set_dim_order:
            ds_IcD = ds_IcD.transpose(*set_dim_order, missing_dims="ignore")

        return ds_IcD


    def _print_verbose(verbose=1, message="", verbose_stage=1):
        """Prints message depending on verbosity"""
        if verbose >= verbose_stage:
            print(message)
        return



