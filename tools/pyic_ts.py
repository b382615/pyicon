#!/usr/bin/env python

import argparse
import json

help_text = """
Figure of timeseries of ICON data.
"""

# --- read input arguments
parser = argparse.ArgumentParser(description=help_text, formatter_class=argparse.RawTextHelpFormatter)

# --- necessary arguments
parser.add_argument('data', nargs='+', metavar='data', type=str,
                    help='Path to ICON data file or simulation name of intake catalog.')
parser.add_argument('var', metavar='var', type=str,
                    help='Name of variable which should be plotted.')
# --- optional arguments
# --- figure saving / showing
parser.add_argument('--fpath_fig', type=str, default='none',
                    help='Path to save the figure.')
parser.add_argument('--dontshow', action='store_true', default=False,
                    help='If dontshow is specified, the plot is not shown')

parser.add_argument('--xlim', type=str, default='auto',
                    help='X-axes limits of the plot. If \'auto\' is specified limits are derived automatically.')
parser.add_argument('--ylim', type=str, default='auto',
                    help='Y-axes limits of the plot. If \'auto\' is specified limits are derived automatically.')
parser.add_argument('--time', type=str, default='auto',
                    help='Time range for selecting data. If \'auto\' is specified limits are derived automatically.')
parser.add_argument('--mean', type=str, default='none',
                    help='Time period of which the data should be resampled and averaged. If \'none\' is specified no averaging is done.')
parser.add_argument('--omit_last_file', action='store_true', default=False,
                    help='If omit_last_file is specified, the last file of the list from the widecard is not used.')
parser.add_argument('--marker', type=str, default=None,
                    help='Specify marker for line plot. If \'None\' is specified no marker is plotted.')

# --- manupilation of data
parser.add_argument('--factor', type=float, default=None,
                    help='Factor to mulitply data with.')

# --- Intake catalog
parser.add_argument('--catalog', type=str, 
                    default='https://data.nextgems-h2020.eu/catalog.yaml',
                    help='Intake catalog which contains simulation.')
parser.add_argument('--model', type=str, 
                    default='ICON',
                    help='Model which to choose from catalog.')
parser.add_argument('--catdict', type=json.loads,
                    help='Dictionary to reduce intake catalog, e.g. like this \'{"time": "P1D", "zoom": 7}\'.')

iopts = parser.parse_args()

print('Start loading modules')
import matplotlib
if iopts.dontshow:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import glob
import os
import sys
from pathlib import Path
from ipdb import set_trace as mybreak  
#sys.path.append(f'{Path.home()}/pyicon/')
import pyicon as pyic  
print('Done loading modules.')

def str_to_array(string):
  string = string.replace(' ', '')
  array = np.array(string.split(','), dtype=float)
  return array

# --- limits
if iopts.xlim!='auto':
  iopts.xlim = str_to_array(iopts.xlim)
if iopts.ylim!='auto':
  iopts.ylim = str_to_array(iopts.ylim)
if iopts.time!='auto':
  iopts.time = np.array(iopts.time.reaplce(' ', '').split(','))

# --- check whether run name or path to data is specified
if iopts.data[0].endswith('.nc'):
  fpath_data = iopts.data
else:
  run = iopts.data[0]
  fpath_data = 'none'

if fpath_data!='none':
  flist = fpath_data
  flist.sort()
  if iopts.omit_last_file:
    flist = flist[:-1]
  ds = xr.open_mfdataset(flist)
else:
  import intake
  cat = intake.open_catalog(iopts.catalog)
  if iopts.catdict:
    reduced_cat = cat[iopts.model][run](**iopts.catdict)
  else:
    reduced_cat = cat[iopts.model][run](zoom=7)
  ds = reduced_cat.to_dask()
data = ds[iopts.var]

# --- select time coordinate
if isinstance(type(iopts.time), np.ndarray):
  data = data.sel(time=slice(iopts.time[0], iopts.time[1]))

# --- apply factor
if iopts.factor:
  data *= iopts.factor

# --- 
if iopts.mean!='none':
  data = data.resample(time=iopts.mean).mean(keep_attrs=True)

# -------------------------
# main plotting command
# -------------------------
P = pyic.Plot(1, 1, plot_cb=False, dfigb=0.5, axlab_kw=None)
ax, cax = P.next()

data.plot(ax=ax, marker=iopts.marker)
ax.grid(True)
ax.set_title(f'{data.long_name} / {data.units}')

# --- save figure
fpath_fig = iopts.fpath_fig
if fpath_fig!='none':
  if fpath_fig.startswith('~'):
    home = str(Path.home())+'/'
    fpath_fig = home + fpath_fig[1:]
  print(f'Saving figure {fpath_fig}...')
  plt.savefig(fpath_fig)

# --- show figure
if not iopts.dontshow:
  plt.show()
