#!/bin/bash
#SBATCH --job-name=pyicon_qp
#SBATCH --time=00:30:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=1
#SBATCH --partition=compute,compute2
#SBATCH --account=mh0033

module list
source ./conda_act_dwd_pyicon_env.sh
which python

rand=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w 3 | head -n 1)

path_pyicon=`(cd .. && pwd)`"/"
config_file="./config_qp_${rand}.py"
qp_driver="${path_pyicon}pyicon/quickplots/qp_driver.py"

cat > ${config_file} << %eof%
# --- path to quickplots
path_quickplots = '../all_qps/'

# --- set this to True if the simulation is still running
omit_last_file = False

# --- do ocean and/or atmosphere plots
do_atmosphere_plots = True
do_conf_dwd         = True
do_djf              = False
do_jja              = False
do_conf_lam         = True
do_ocean_plots      = False

time_at_end_of_interval = True
shift_timeseries        = True # important for monthly time series!!

# --- grid information
gname     = 'R2B4-R2B4'
lev       = 'L40'
#gname_atm = 'r2b4_atm_r0012'
gname_atm = 'icon-clm'
lev_atm   = 'L90'

# --- for plotting
projection     = 'RotatedPole'

# --- path to interpolation files
path_grid        = '/hpc/uwork/icon-sml/pyICON/grids/'+gname+'/'
path_grid_atm    = '/hpc/uwork/icon-sml/pyICON/grids/'+gname_atm+'/'
path_ckdtree     = path_grid+'/ckdtree/'
path_ckdtree_atm = path_grid_atm+'/ckdtree/'
rgrid_name_atm   = 'regional_0.25_europe011_rotated' # sname !

# --- grid files and reference data
path_pool_oce       = '/hpc/uwork/icon-sml/pyICON/grids/'
gnameu = gname.split('_')[0].upper()
fpath_tgrid                 = path_grid + gname+'_tgrid.nc'
fpath_tgrid_atm             = path_grid_atm + gname_atm+'_tgrid.nc'
fpath_ref_data_oce          = path_grid + 'ts_phc3.0_annual_icon_grid_0043_R02B04_G_L40.nc'
fpath_ref_data_atm          = path_grid_atm + 'era5_pyicon_2000-2005_europe025_rotated.nc'
fpath_ref_data_atm_djf      = path_grid_atm + 'era5_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_jja      = path_grid_atm + 'era5_pyicon_2001-2010_jja_1.5x1.5deg.nc'
#fpath_ref_data_atm_rad      = path_grid_atm + 'ceres_pyicon_2001-2010_1.5x1.5deg.nc'
fpath_ref_data_atm_rad_djf  = path_grid_atm + 'ceres_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_rad_jja  = path_grid_atm + 'ceres_pyicon_2001-2010_jja_1.5x1.5deg.nc'
#fpath_ref_data_atm_prec     = path_grid_atm + 'gpm_pyicon_2001-2010_1.5x1.5deg.nc'
fpath_ref_data_atm_prec_djf = path_grid_atm + 'gpm_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_prec_jja = path_grid_atm + 'gpm_pyicon_2001-2010_jja_1.5x1.5deg.nc'
fpath_fx                    = path_grid + 'oce_fx.19600102T000000Z.nc'

# --- nc file prefixes ocean
oce_def     = '_oce_def'
oce_moc     = '_oce_moc'
oce_mon     = '_oce_mon'
oce_ice     = '_oce_ice'
oce_monthly = '_oce_dbg'

# --- nc file prefixes atmosphere
atm_2d      = '_atm_2d_ml'
atm_3d      = '_atm_3d_ml'
atm_mon     = '_atm_mon'

# --- nc output
save_data = False
path_nc = '/scratch/m/m300602/tmp/test_pyicon_output/'

# --- time average information (can be overwritten by qp_driver call)
tave_ints = [
#['1630-02-01', '1640-01-01'],
['4450-02-01', '4500-01-01'],
]
ave_freq = 0

# --- decide if time-series (ts) plots are plotted for all the 
#     available data or only for the intervall defined by tave_int
use_tave_int_for_ts = True

# --- what to plot and what not?
# --- not to plot:
#red_list = ['atm_toa_sod','atm_toa_sod_bias','atm_toa_sou','atm_toa_sou_bias',
#            'atm_relhum_700','atm_relhum_700_bias']
# --- to plot:
green_list = ['atm_tauu', 'atm_tauu_bias', 'atm_tauv', 'atm_tauv_bias',
              'atm_t2m', 'atm_t2m_bias','atm_ts', 'atm_ts_bias',
              'atm_surf_shfl', 'atm_surf_shfl_bias', 'atm_surf_lhfl', 'atm_surf_lhfl_bias',
              'atm_toa_sob', 'atm_toa_sob_bias', 'atm_toa_thb', 'atm_toa_thb_bias',
              'atm_sfc_sob', 'atm_sfc_sob_bias', 'atm_sfc_thb', 'atm_sfc_thb_bias',
              'atm_cwv', 'atm_cwv_bias', 'atm_tcc', 'atm_tcc_bias',
              'atm_tp', 'atm_tp_bias', 'atm_pme', 'atm_pme_bias',
              'sea_ts', 'seaice_fraction',
              'atm_psl', 'atm_psl_bias', 'atm_w10m', 'atm_w10m_bias',
              'ts_t2m_gmean', 'ts_radtop_gmean','ts_rsdt_gmean', 'ts_rsut_gmean',
              'ts_rlut_gmean', 'ts_prec_gmean', 'ts_evap_gmean', 'ts_pme_gmean']
%eof%

# --- start qp_driver
startdate=`date +%Y-%m-%d\ %H:%M:%S`

run="Nukleus_iconOnly_031"
path_data="/hpc/uwork/icon-sml/pyICON/testdata/icon-clm/EUR11/"
python -W ignore -u ${qp_driver} --batch=True ${config_file} --path_data=$path_data --run=$run \
       --tave_int='2000-01-01,2001-01-01'

enddate=`date +%Y-%m-%d\ %H:%M:%S`

rm ${config_file}

echo "--------------------------------------------------------------------------------"
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

