#!/usr/bin/env python

import argparse
import json

help_text = """
"""
# --- read input arguments
parser = argparse.ArgumentParser(description=help_text, formatter_class=argparse.RawTextHelpFormatter)

# --- necessary arguments
parser.add_argument('fpath_data', nargs='+', metavar='data', type=str,
                    help='Path to ICON data file or simulation name of intake catalog.')
# --- optional arguments
# --- figure saving / showing
parser.add_argument('--fpath_fig', type=str, default='none',
                    help='Path to save the figure.')
parser.add_argument('--dontshow', action='store_true', default=False,
                    help='If dontshow is specified, the plot is not shown')
# --- selecting time
parser.add_argument('--it', type=int, default=0,
                    help='Time index which should be plotted.')
parser.add_argument('--time', type=str, default='none',
                    help='Time string \'yyyy-mm-dd\' wich should be plotted (if specified overwrites \'it\').')

parser.add_argument('--lon_reg', type=str, default=None,
                    help='Longitude range of the plot.')
parser.add_argument('--lat_reg', type=str, default=None,
                    help='Latitude range of the plot.')
parser.add_argument('--lon_reg_ell', type=str, default=None,
                    help='Longitude range for the data of the ellipse plot.')
parser.add_argument('--lat_reg_ell', type=str, default=None,
                    help='Latitude range for the data of the ellipse plot.')
parser.add_argument('--conc_min', type=float, default=0.5,
                    help='Filter ellpise point with minimum concentration.')

iopts = parser.parse_args()

print('Start loading modules')
import matplotlib
if iopts.dontshow:
  matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import glob
import os
import sys
from pathlib import Path
from ipdb import set_trace as mybreak  
#sys.path.append(f'{Path.home()}/pyicon/')
import pyicon as pyic  
import cartopy.crs as ccrs
import cmocean
print('Done loading modules.')

def str_to_array(string):
  string = string.replace(' ', '')
  array = np.array(string.split(','), dtype=float)
  return array

# --- limits
if iopts.lon_reg:
  iopts.lon_reg = str_to_array(iopts.lon_reg)
if iopts.lat_reg:
  iopts.lat_reg = str_to_array(iopts.lat_reg)
if iopts.lat_reg_ell:
  iopts.lat_reg = str_to_array(iopts.lat_reg_ell)

if iopts.lat_reg is None:
  iopts.lat_reg = [70, 90]
if iopts.lon_reg is None:
  iopts.lon_reg = [-180, 180]
if iopts.lat_reg_ell is None:
  iopts.lat_reg_ell = [85, 90]
if iopts.lon_reg_ell is None:
  iopts.lon_reg_ell = [-180, 180]

# --- open dataset and select data
mfdset_kwargs = dict(combine='nested', concat_dim='time', 
                     data_vars='minimal', coords='minimal', 
                     compat='override', join='override', parallel=True)
ds = xr.open_mfdataset(iopts.fpath_data, **mfdset_kwargs, chunks={'time': 1})
ds = ds.isel(lev=0)

# --- select time coordinate
if 'time' in ds.dims:
  if iopts.time=='none':
    ds = ds.isel(time=iopts.it)
  else:
    ds = ds.sel(time=iopts.time, method='nearest')

clon = np.rad2deg(ds.clon.compute())
clat = np.rad2deg(ds.clat.compute())

hi = ds.hi.compute()
conc = ds.conc.compute()
sigma_i = ds.sigma_i.compute()
sigma_ii = ds.sigma_ii.compute()
Delta = ds.Delta.compute()
s11 = ds.s11.compute()
s22 = ds.s22.compute()
s12 = ds.s12.compute()
s21 = ds.s21.compute()

# --- ellipse
C = 20.
Pstar = 27.5e3 # N/m^2
Pp = Pstar * hi*conc * np.exp(-C*(1-conc))

s1 = 0.5*(s11+s22) + np.sqrt((0.5*(s11-s22))**2 + s12**2)
s2 = 0.5*(s11+s22) - np.sqrt((0.5*(s11-s22))**2 + s12**2)
s1 = s1.assign_attrs({'uuidOfHGrid': ds.uuidOfHGrid})
s2 = s2.assign_attrs({'uuidOfHGrid': ds.uuidOfHGrid})

s1_n = s1 / (Pp+1e-12)
s2_n = s2 / (Pp+1e-12)

mask = (
     (clat>=iopts.lat_reg[0]) & (clat<iopts.lat_reg[1]) 
  &  (clon>=iopts.lon_reg[0]) & (clon<iopts.lon_reg[1]) 
  & (s1_n!=0) & (s2_n!=0)
  & (conc>iopts.conc_min)
)
#s1_n = s1_n.where(mask)
#s2_n = s2_n.where(mask)

#mask = (s1_n!=0) & (s2_n!=0)
hist, s2bins, s1bins = np.histogram2d(s2_n[mask], s1_n[mask], bins=2000, range=[[-5,5], [-5,5]], density=True)

# -------------------------
# --- plotting
# -------------------------
plt.close('all')

proj = ccrs.NorthPolarStereo()

projs = [proj]*9
projs[-1] = None
P = pyic.Plot(3, 3, projection=projs, asp=1, fig_size_fac=1, sharex=False, sharey=False, dfigt=0.1)

lat_reg = [80,90]
res = 0.1

P.next()
P.plot(hi, clim=[0, 2], lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$h_i$')

P.next()
P.plot(conc, clim=[0.9,1], lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$conc$')

P.next()
P.plot(Pp, clim=4e4, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$P_p$')

P.next()
P.plot(s11, clim=2e4, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$\sigma_{11}$')

# P.next()
# P.plot(s22, clim=2e4, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
# P.ax.set_title('$\sigma_{22}$')

P.next()
P.plot(s12, clim=5e3, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$\sigma_{12}$')

P.next()
P.plot(sigma_i, clim=5e4, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$\sigma_I = \sigma_{11} + \sigma_{22}$')

P.next()
P.plot(s1_n, clim=1.2, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$\sigma_1 / P_p = \frac{\sigma_I}{2} + \sqrt{(\frac{\sigma_{II}}{2})^2-\sigma_{12}^2}) / P_p$')

# P.next()
# P.plot(s2_n, clim=1.2, lon_reg=[-180,180], lat_reg=lat_reg, res=res)
# P.ax.set_title('$\sigma_2 / P_p$')

P.next()
P.plot(Delta, clim=[0,4e-6], lon_reg=[-180,180], lat_reg=lat_reg, res=res)
P.ax.set_title('$\Delta$')

#---
ax, cax = P.next()
P.shade(s1bins, s2bins, hist, clim=[0,5], cmap='Blues')
xylim = 2

s1_ov_P = np.linspace(-1.5, 0.5, 101)
s2_ov_P = np.linspace(-1.5, 0.5, 101)

S1, S2 = np.meshgrid(s1_ov_P, s2_ov_P)

e = 2.
Z = (S1+S2+1)**2 + e**2*(S1-S2)**2

ax.contour(s1_ov_P, s2_ov_P, Z, [1], colors='r')

ax.text(0.05, 0.85, 'lat_reg = '+str(iopts.lat_reg_ell)+'\nlon_reg = '+str(iopts.lon_reg_ell), fontsize=8, transform=ax.transAxes)
ax.set_xlim(-1.5, 0.5)
ax.set_ylim(-1.5, 0.5)
ax.set_xlabel('$\sigma_1 / P_p$')
ax.set_ylabel('$\sigma_2 / P_p$')

for ax in P.hca:
    ax.set_title('', loc='left')
    ax.set_title('', loc='right')

runname = os.path.abspath(iopts.fpath_data[0]).split('/')[-2]
plt.text(0.02, 0.02, runname, fontsize=8, transform=plt.gcf().transFigure)
plt.text(0.02, 0.98, str(ds.time.data)[:-10], fontsize=8, transform=plt.gcf().transFigure)

# --- save figure
fpath_fig = iopts.fpath_fig
if fpath_fig!='none':
  if fpath_fig.startswith('~'):
    home = str(Path.home())+'/'
    fpath_fig = home + fpath_fig[1:]
  print(f'Saving figure {fpath_fig}...')
  plt.savefig(fpath_fig)

# --- show figure
if not iopts.dontshow:
  print('Showing figure')
  plt.show()
