#!/bin/bash

# This should coincide with pyicon/version.py
version=0.2.0

git tag ${version}
git push origin ${version}
