#!/bin/bash

echo "************************************************************" 
echo " 03_pyic_fig_and_sec.sh "
echo "************************************************************" 

path_data=`cat path_data.txt`

../tools/pyic_fig.py ${path_data}/outdata/nib2501_oce_P1M_2d_22000101.nc HeatFlux_Total --fpath_fig=${path_data}/pyicon_output/pyic_fig_example.pdf --dontshow

../tools/pyic_sec.py ${path_data}/outdata/nib2501_oce_P1M_3d_22000101.nc to --section="170W" --fpath_fig=${path_data}/pyicon_output/pyic_sec_example.pdf --dontshow

