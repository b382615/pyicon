#!/bin/bash

set -ex

source_dir="${HOME}/work/pyicon_data"
path_data="${HOME}/work/pyicon_data/swift.dkrz.de/example_data_r2b4"

cat > path_data.txt << EOF
${path_data}
EOF

cat > source_dir.txt << EOF
${source_dir}
EOF

mkdir -p ${path_data}/pyicon_output
cp path_data.txt ${path_data}/pyicon_output/

./01_download_example_data.sh
./02_create_ckdtree.sh
./03_pyic_fig_and_sec.sh
./04_notebook_plotting.sh
./05_notebook_calculations.sh
./06_quickplots.sh

echo "Output written to ${source_dir}/swift.dkrz.de/example_data_r2b4/pyicon_output/"
