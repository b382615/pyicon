runname = 'ocean_era51h_r2b8_19074-AMK'
run = 'ocean_era51h_r2b8_19074-AMK'
tstep = '20120301T000000Z'

path_data    = '/work/mh0033/m211054/projects/icon/icon-oes-1.0.08/icon-oes-1.0.08/experiments/'+runname+'/outdata/'
fpath_ref_data_oce = path_data+'../initial_state.nc'
gname = 'OceanOnly_IcosSymmetric_4932m_rotatedZ37d_modified_srtm30_1min'
path_tgrid   = '/pool/data/ICON/oes/input/r0002/'+gname+'/'
fpath_tgrid   = '/pool/data/ICON/oes/input/r0002/'+gname+'/'+gname+'.nc' 
path_ckdtree = '/mnt/lustre01/work/mh0033/m300602/proj_vmix/icon/icon_ckdtree/'
fpath_fx = '/mnt/lustre01/work/mh0033/m211054/projects/icon/FX/R2B8/ocean_omip_long_r2b8_18250-XMO_fx.nc'

