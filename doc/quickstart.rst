Quickstart
==========

Prepare example data
--------------------

Download example data to an arbitrary directory::

  wget -r -H -N --cut-dirs=3 --include-directories="/v1/" "https://swiftbrowser.dkrz.de/public/dkrz_07387162e5cd4c81b1376bd7c648bb60/pyicon_example_data/?show_all" p

First glance at the data with pyicon-tools
------------------------------------------

You can use pyicon's command line tools to visualize the data. To do this, we are assuming in the following, that you link `pyic_fig.py` and `pyic_sec.py` to a directory which is in your search path. Otherwise, you need to replace `pyic_fig.py` with `/path/to/pyicon/tools/pyic_fig.py`.

First go to the directory of the example data::

   cd swift.dkrz.de/example_data_r2b4/outdata

Plot some data via::

   pyic_fig.py nib2501_oce_P1M_3d_22000101.nc to

If you want more info how to select data and change the plotting appearance, please type::

   pyic_fig.py -h

You can also plot sections and zonal averages via::

   pyic_sec.py nib2501_oce_P1M_3d_22000101.nc to --section=170W

More analysis with pyicon-core
------------------------------

You can also use pyicon from within a python script or a Jupyter Notebook, just import it by::

   import pyicon as pyic
   import xarray as xr

Then, load and plot some data::

   ds = xr.open_dataset('/path/to/example_data/nib2501_oce_P1M_3d_22000101.nc')
   ds.to.isel(time=0, depth=0).pyic.plot()

More plotting examples can be found here::

   https://m300602.gitlab-pages.dkrz.de/pyicon/notebooks_nblink/examp_easyms2022.html

If you want to derive divergences, gradient and curl on the native grid, please have a look into this notebook::

   https://gitlab.dkrz.de/m300602/pyicon/-/blob/master/notebooks/examp_oce_density_and_velocity_gradients.ipynb
   https://gitlab.dkrz.de/m300602/pyicon/-/blob/master/notebooks/examp_oce_reconstructions.ipynb

