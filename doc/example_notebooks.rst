=================
Example notebooks
=================

In this section, we introduce examples of using pyicon. In ``Introduction`` we cover some basics of python and pyicon. Then, we have three subsections ``Atmosphere``, ``Ocean``, and ``Ocean-Atmosphere`` covering the usage of pyicon specifically for the ocean and the atmosphere. In the section ``Plotting`` we show how to manage axes and grids. Finally, in the section ``View``, we cover a pyicon specific, pyicon view, that aims to reproduce some ncview-like plotting options. A great overview of pyicon is the notebook ``Basic plotting of ICON data``, and we recommend going through this notebook. 


Introductory notebooks
======================

In this section, you can find introduction notebooks. If you are already experienced in python, we advise you to start with ``Basic plotting of ICON data``. 
For beginners, we offer a basic python introduction ``Welcome to a short introduction to python``. If you are already experienced in python and only new to pyicon, you can start with ``Starter example for pyicon``. 

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/python_introduction.ipynb
   notebooks_nblink/examp_intro_healpix.ipynb
   notebooks_nblink/examp_easyms2022.ipynb
   notebooks_nblink/examp_intro_plot_options.ipynb
   notebooks_nblink/examp_intro_start.ipynb
   notebooks_nblink/examp_intro_start_xr.ipynb


Atmosphere
==========

Here you can find notebooks of examples where pyicon is used for atmosphere.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/examp_atm_2d_scalars.ipynb
   notebooks_nblink/examp_atm_converting_omega_to_w.ipynb
   notebooks_nblink/examp_atm_equation_of_state.ipynb
   notebooks_nblink/examp_atm_geopotential_height.ipynb
   notebooks_nblink/examp_atm_plot_zonally_averaged_data.ipynb
   notebooks_nblink/examp_atm_plotting_era_data.ipynb
   notebooks_nblink/examp_atm_wind_stress.ipynb
   notebooks_nblink/examp_atm_wind_stress_curl.ipynb

Ocean
=====

Here you can find notebooks of examples where pyicon is used for ocean.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/examp_intro_start.ipynb
   notebooks_nblink/examp_intro_start_xr.ipynb
   notebooks_nblink/examp_oce_biases.ipynb
   notebooks_nblink/examp_oce_density_and_velocity_gradients.ipynb
   notebooks_nblink/examp_oce_geostrophic_velocities.ipynb
   notebooks_nblink/examp_oce_reconstructions.ipynb
   notebooks_nblink/examp_oce_timeseries.ipynb
   notebooks_nblink/examp_oce_transports.ipynb
   notebooks_nblink/examp_oce_uvw_from_mass_flux.ipynb
   notebooks_nblink/examp_oce_vert_grad_unequal_spacing.ipynb
   notebooks_nblink/examp_oce_zstar.ipynb

Ocean Atmposphere
=================

 

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/examp_oceatm_crop_domain.ipynb
   notebooks_nblink/examp_oceatm_save_netcdf.ipynb
   notebooks_nblink/examp_oceatm_time_averaging_with_dask.ipynb

Plotting
========

The following notebooks are the examples of plotting with pyicon, mainly covering how to modify axes and grids.  

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/examp_plotting_arrange_axes.ipynb
   notebooks_nblink/examp_plotting_dual_and_primal_grid.ipynb
   notebooks_nblink/examp_plotting_map_projections.ipynb
   notebooks_nblink/examp_plotting_split_axes_vertically.ipynb

View
====
In this section we cover pyicon view that aims to reproduce some ncview like plotting options. This allows loading an ICON data set and click through the time or depth axes. All variables within the file can be depicted, color limits can be set and the plot can be manipulated e.g. by adding Rectangles.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks_nblink/pyicon_view_cdo_remapped.ipynb
   notebooks_nblink/pyicon_view_nextgems.ipynb
   notebooks_nblink/pyicon_view_nextgems_xr.ipynb 
   notebooks_nblink/pyicon_view_nextgems_xr_intake.ipynb
   notebooks_nblink/pyicon_view_nextgems_xr_intake_minimum_example.ipynb
   notebooks_nblink/pyicon_view_r2b6.ipynb
   notebooks_nblink/pyicon_view_r2b8.ipynb
   notebooks_nblink/pyicon_view_smt.ipynb
   
