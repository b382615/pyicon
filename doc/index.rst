.. image:: _static/logo2.png

*******************************
python diagnostic tool for ICON
*******************************

Pyicon is a python post-processing and visualization toolbox for ICON with a focus on ocean data. The three main features of pyicon are:

* :ref:`pyicon-core`: a number of functions to facilitate the every-day script-based plotting of ICON data
* :ref:`pyicon-quickplots`: a monitoring suite for ICON ocean simulations which combines dedicated diagnostic plots of an ICON simulation on a website
* :ref:`pyicon-view`: an interactive (ncview-like) plotting GUI for Jupyter notebook
* :ref:`pyicon-tools`: command line based set of scripts

Pyicon is developed by the Max Planck Institute for Meterology and the University of Hamburg within the DFG-project TRR181 - Energy Transfers in Atmosphere and Ocean.

Pyicon is hosted here: `<https://gitlab.dkrz.de/m300602/pyicon/>`_

.. toctree::
   :maxdepth: 1 
   :caption: Contents:

   installation
   quickstart
   pyicon-core
   pyicon-tools
   pyicon-quickplots
   pyicon-view
   example_notebooks
   api
  
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
